//
//  General.swift
//  weather-geo
//
//  Created by Konstantin Igorevich on 17.11.2019.
//  Copyright © 2019 Konstantin Igorevich. All rights reserved.
//

import Foundation

class General {
    static let appid = "a1ff53aa479d183bacac184fd1e84738"
    static let baseUrl = "https://api.openweathermap.org/data/2.5/forecast?"
    static let iconUrl = "http://openweathermap.org/img/wn/"
    static let weatherUnits = "metric"
}
