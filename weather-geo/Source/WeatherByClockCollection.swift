//
//  WeatherByClockCollection.swift
//  weather-geo
//
//  Created by Konstantin Igorevich on 18.11.2019.
//  Copyright © 2019 Konstantin Igorevich. All rights reserved.
//

import UIKit

class WeatherByClockCollection: BaseViewController {
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var weatherModel: WeatherModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configuredUI()
    }
    
    func reloadDataWith(weather: WeatherModel) {
        weatherModel = weather
        collectionView.reloadData()
    }
    
    func scrollCollectionToItemWith(data: String) {
        if let i = weatherModel?.list?.firstIndex(where: { ($0.dtTxt?.hasPrefix(data) ?? false) }) {
            print("\(i)")
            collectionView.scrollToItem(at: IndexPath(item: i, section: 0), at: .left, animated: true)
        }
    }
    
    private func configuredUI() {
        collectionView.layer.cornerRadius = 15
    }
}


// MARK: - UICollectionViewDataSource

extension WeatherByClockCollection: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return weatherModel?.list?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let weatherCell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.weatherCollectionCell, for: indexPath) else { return UICollectionViewCell() }
        guard let item = weatherModel?.list?[indexPath.item] else { return UICollectionViewCell() }
        weatherCell.setupCellWith(weather: item)
        
        return weatherCell
    }
}


// MARK: - UICollectionViewDelegateFlowLayout

extension WeatherByClockCollection: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 140, height: 140
        )
    }
}


// MARK: - UICollectionViewDelegate

extension WeatherByClockCollection: UICollectionViewDelegate {}
