//
//  MapController.swift
//  weather-geo
//
//  Created by Konstantin Igorevich on 17.11.2019.
//  Copyright © 2019 Konstantin Igorevich. All rights reserved.
//

import UIKit
import MapKit

class MapController: BaseViewController, UIGestureRecognizerDelegate {

    
    @IBOutlet weak var mapView: MKMapView!
    
    private var annotation = [MKAnnotation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(isClosedWeatherInfoView), name: NSNotification.Name("CloseInfoView"), object: nil)
    }

    @IBAction func tapGesture(_ sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            let locationInMap = sender.location(in: mapView)
            print(locationInMap)
            let tappedCoordinate = mapView.convert(locationInMap , toCoordinateFrom: mapView)
            addAnnotation(location: tappedCoordinate)
            loadWeatherWith(location: tappedCoordinate)
        }
    }
    
    private func addAnnotation(location: CLLocationCoordinate2D){
        let annotation = MKPointAnnotation()
        print(location)
        let span = MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        annotation.coordinate = location
        self.annotation.append(annotation)
        self.mapView.addAnnotation(annotation)
    }
    
    private func loadWeatherWith(location: CLLocationCoordinate2D) {
        self.startLoader()
        NetWorker.shared.loadWeatherWithCoordinate(latitude: location.latitude, longitude: location.longitude) { (weatherModel) in
            if let weather = weatherModel {
                self.openWeatherScreen(with: weather)
            } else {
                self.openErrorScreen()
            }
            self.stopLoader()
        }
    }
    
    private func openWeatherScreen(with weather: WeatherModel) {
        guard let weatherScreen = R.storyboard.main.weatherController() else { return }
        weatherScreen.modalPresentationStyle = .overCurrentContext
        weatherScreen.weatherModel = weather
        if #available(iOS 13.0, *) {
            weatherScreen.overrideUserInterfaceStyle = .light
        } 
        present(weatherScreen, animated: false, completion: nil)
    }
    
    private func openErrorScreen() {
        let alertController = UIAlertController(title: "Упс", message: "К сожалению что то пошло не так!", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "ОК", style: .cancel) { _ in}
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    
    @objc private func isClosedWeatherInfoView() {
        mapView.removeAnnotations(mapView.annotations)
        annotation.removeAll()
    }
}

