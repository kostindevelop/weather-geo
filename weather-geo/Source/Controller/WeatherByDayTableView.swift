//
//  WeatherByDayTableView.swift
//  weather-geo
//
//  Created by Konstantin Igorevich on 18.11.2019.
//  Copyright © 2019 Konstantin Igorevich. All rights reserved.
//

import UIKit

protocol SelectedTableCellProtocol {
    func selectedCellWith(selectedData: String)
}

class WeatherByDayTableView: BaseViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var weatherModel: WeatherModel?
    var weatherList = [[List]]()
    
    var delegate: SelectedTableCellProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func reloadDataWith(weather: WeatherModel) {
        weatherList = sortedWeatherByDays(weather: weather)
        tableView.reloadData()
    }
    
    private func sortedWeatherByDays(weather: WeatherModel) -> [[List]] {
        var sortedWeatherModel = [[List]]()
        let allDays = weather.list?.map({
            
            $0.dtTxt?.convertDateWith(format: "dd MM yy")
            
        })
        let filterRepeatDays = allDays?.uniques ?? []
        for day in filterRepeatDays {
            let weatherOfDay = weather.list?.filter({ $0.dtTxt?.convertDateWith(format: "dd MM yy") == day}) ?? []
            
            sortedWeatherModel.append(weatherOfDay)
        }
        return sortedWeatherModel
    }
    
    private func sortedDublicateElement<S : Sequence, T : Hashable>(source: S) -> [T] where S.Iterator.Element == T {
            var buffer = [T]()
            var added = Set<T>()
            for elem in source {
                if !added.contains(elem) {
                    buffer.append(elem)
                    added.insert(elem)
                }
            }
            return buffer
    }
}

extension Array where Element: Hashable {
    var uniques: Array {
        var buffer = Array()
        var added = Set<Element>()
        for elem in self {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
}

extension WeatherByDayTableView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let weatherTableCell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.weatherTableCell, for: indexPath) else { return UITableViewCell() }
        guard let item = weatherList[indexPath.row].first else { return weatherTableCell}
        weatherTableCell.setupCellWith(weatherList: item)
        return weatherTableCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = weatherList[indexPath.row].first
        delegate?.selectedCellWith(selectedData: item?.dtTxt ?? "")
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

extension WeatherByDayTableView: UITableViewDelegate {}
