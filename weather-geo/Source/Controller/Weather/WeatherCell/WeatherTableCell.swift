//
//  WeatherTableCell.swift
//  weather-geo
//
//  Created by Konstantin Igorevich on 18.11.2019.
//  Copyright © 2019 Konstantin Igorevich. All rights reserved.
//

import UIKit

class WeatherTableCell: UITableViewCell {
    
    @IBOutlet weak var weatherData: UILabel!
    @IBOutlet weak var weatherTempMin: UILabel!
    @IBOutlet weak var weatherTempMax: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCellWith(weatherList: List) {
        weatherData.text = weatherList.dtTxt?.convertDateWith(format: "dd MMMM")
        weatherTempMin.text = "min: \(weatherList.main?.tempMin ?? 0.0)"
        weatherTempMax.text = "max: \(weatherList.main?.tempMax ?? 0.0)"
    }

}
