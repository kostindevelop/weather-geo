//
//  WeatherCollectionCell.swift
//  weather-geo
//
//  Created by Konstantin Igorevich on 18.11.2019.
//  Copyright © 2019 Konstantin Igorevich. All rights reserved.
//

import UIKit

class WeatherCollectionCell: UICollectionViewCell {
    
    
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var sky: UILabel!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var wind: UILabel!
    @IBOutlet weak var time: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configuredCell()
    }
    
    func setupCellWith(weather: List) {
        let icon = weather.weather?.first?.icon
        let imageUrl = General.iconUrl + "\(icon ?? "")@2x.png"
        weatherIcon.load(url: URL(string: imageUrl)!)
        self.sky.text = weather.weather?.first?.weatherDescription
        let temp = weather.main?.temp
        self.temperature.text = "\(temp ?? 0.0)C"
        self.wind.text = "\(weather.wind?.speed ?? 0.0)m/s"
        self.time.text = weather.dtTxt?.convertDateWith(format: "dd MMM hh:mm")
    }
    
    private func configuredCell() {
        contentView.layer.cornerRadius = 10
        contentView.layer.borderColor = UIColor.clear.cgColor
        contentView.layer.masksToBounds = true
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0.5)
        layer.shadowRadius = 6.0
        layer.shadowOpacity = 0.1
        layer.masksToBounds = false
    }
    
}
