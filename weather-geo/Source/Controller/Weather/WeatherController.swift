//
//  WeatherController.swift
//  weather-geo
//
//  Created by Konstantin Igorevich on 17.11.2019.
//  Copyright © 2019 Konstantin Igorevich. All rights reserved.
//

import UIKit

class WeatherController: BaseViewController {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var weatherView: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    var weatherModel: WeatherModel?
    
    let weatherCollection = WeatherByClockCollection()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configuredUI()
        setupWeatherData(weather: weatherModel!)
    }
    
    private func setupWeatherData(weather: WeatherModel) {
        let cityName = weather.city?.name
        let countryCode = weather.city?.country
        cityLabel.text = "- \(countryCode ?? ""), \(cityName ?? "") -"
        let collectionVC = children[0] as? WeatherByClockCollection
        collectionVC?.reloadDataWith(weather: weather)
        let tableVC = children[1] as? WeatherByDayTableView
        tableVC?.delegate = self
        tableVC?.reloadDataWith(weather: weather)
    }
    
    
    private func isHiddenWeatherView(isHidden: Bool) {
        UIView.animate(withDuration: 0.43, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 2, options: [], animations: {
            self.bottomConstraint.constant = isHidden ? -self.weatherView.frame.height : 0
            self.view.layoutIfNeeded()
        }) { _ in
            if isHidden {
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    private func configuredUI() {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.isHiddenWeatherView(isHidden: false)
        }
        weatherView.layer.cornerRadius = 15
        bottomConstraint.constant = -weatherView.frame.height
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        NotificationCenter.default.post(name: NSNotification.Name("CloseInfoView"), object: nil)
        isHiddenWeatherView(isHidden: true)
    }
}

extension WeatherController: SelectedTableCellProtocol {
    func selectedCellWith(selectedData: String) {
        let collectionVC = children[0] as? WeatherByClockCollection
        collectionVC?.scrollCollectionToItemWith(data: selectedData)
    }
}
