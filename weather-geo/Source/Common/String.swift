//
//  String.swift
//  weather-geo
//
//  Created by Konstantin Igorevich on 19.11.2019.
//  Copyright © 2019 Konstantin Igorevich. All rights reserved.
//

import Foundation

extension String {
    func convertDateWith(format: String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatterPrint = DateFormatter()
//        dateFormatterPrint.locale = Locale(identifier: "ru")
        dateFormatterPrint.dateFormat = format
        
        if let stringDate = dateFormatterGet.date(from: self) {
            let convertDate = dateFormatterPrint.string(from: stringDate)
            return convertDate
        } else {
            return ""
        }
    }
}
