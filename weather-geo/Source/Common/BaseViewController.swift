//
//  BaseViewController.swift
//  weather-geo
//
//  Created by Konstantin Igorevich on 18.11.2019.
//  Copyright © 2019 Konstantin Igorevich. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class BaseViewController: UIViewController, NVActivityIndicatorViewable {
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    func startLoader() {
        startAnimating(CGSize(width: 200, height: 200),type: .ballScaleRippleMultiple, color: .blue)
    }
    
    func stopLoader() {
        stopAnimating()
    }
    
}
