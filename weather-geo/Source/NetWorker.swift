//
//  NetWorker.swift
//  weather-geo
//
//  Created by Konstantin Igorevich on 17.11.2019.
//  Copyright © 2019 Konstantin Igorevich. All rights reserved.
//

import Foundation
import Alamofire

class NetWorker {
    
    static let shared = NetWorker()

    func loadWeatherWithCoordinate(latitude: Double, longitude: Double, completion: @escaping (WeatherModel?) -> Void) {
        let urlString = General.baseUrl + "lat=\(latitude)&lon=\(longitude)&units=\(General.weatherUnits)&appid=\(General.appid)"
        
        Alamofire.request(urlString, method: .get)
            .validate()
            .responseData { response in
                guard response.result.isSuccess else {
                    completion(nil)
                    return
                }
                
                guard let data = response.result.value else {
                    completion(nil)
                    return
                }
                
                let weatherModel = try? JSONDecoder().decode(WeatherModel.self, from: data)
                DispatchQueue.main.async {
                    completion(weatherModel)
                }
        }

    }
       
    
}
